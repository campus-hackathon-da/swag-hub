package campushackathon.api;

import campushackathon.data.RepositoryDto;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public interface RepositoryController {
    @GetMapping("/repositories")
    List<RepositoryDto> getRepositories();
}
