package campushackathon.api;

import campushackathon.data.RepositoryDto;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RepositoryControllerImpl implements RepositoryController {

    @Override
    public List<RepositoryDto> getRepositories() {
        List<RepositoryDto> repositoryDtos = new ArrayList<>();
        repositoryDtos.add(new RepositoryDto("Sprint Boot", "https://github.com/spring-projects/spring-boot", "Java" ,0.89));
        repositoryDtos.add(new RepositoryDto("Test", "https://github.com", "Java", 0.78));

        return repositoryDtos;
    }
}
