package campushackathon.data;

public class RepositoryDto {

    private final String name;
    private final String url;
    private final String type;
    private final double trend;


    public RepositoryDto(String name, String url, String type, double trend) {
        this.name = name;
        this.url = url;
        this.type = type;
        this.trend = trend;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getType() {
        return type;
    }

    public double getTrend() {
        return trend;
    }
}
