package io.swaghub.rank

import org.springframework.stereotype.Component
import java.util.*

@Component
class SimpleCalculator{

    fun calculateRank(values: List<DataSet>): Double{

        val niceFactorList: ArrayList<Double> = ArrayList()

        for ((index, value) in values.sortedWith(compareBy({ it.timeStamp })).withIndex()){
            if(index!=0){
                val starsBefore = values[index - 1].stars
                val increase = value.stars - starsBefore
                val niceTry = (increase.toDouble() / starsBefore.toDouble())
                niceFactorList.add(niceTry)
            }
        }

        var niceFactor = niceFactorList.sum()
        niceFactor /= niceFactorList.size
        return niceFactor
    }

}