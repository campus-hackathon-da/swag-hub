package io.swaghub.rank

import org.apache.commons.csv.CSVFormat
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import java.io.FileReader

private val log = LoggerFactory.getLogger(SimpleCalculator::class.java)

@Service
class SimpleCalcService {

    @Autowired
    lateinit var simpleCalculator: SimpleCalculator
    
    fun calcSimple(projectId: String): SimpleResponse {
        log.info("Calc {}", projectId)
        return SimpleResponse(simpleCalculator.calculateRank(readDataSet()))
    }

}

fun readDataSet(): List<DataSet> {
    val result: ArrayList<DataSet> = ArrayList()
    val file = SimpleCalcService::class.java.classLoader.getResource("train.csv").file
    val input = FileReader(file)
    val records = CSVFormat.EXCEL.parse(input)
    records.mapTo(result) { DataSet(Integer.parseInt(it.get(0)), Integer.parseInt(it.get(1))) }
    return result
}

class SimpleResponse(val value: Double)